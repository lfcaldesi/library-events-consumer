package com.kafka.entity;

import com.sun.istack.NotNull;
import lombok.*;


import javax.persistence.*;


@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@Entity
public class LibraryEvent {

@Id
@GeneratedValue
    private Integer libraryEventId;

@Enumerated(EnumType.STRING)
    private LibraryEventType libraryEventType;

@OneToOne(mappedBy = "libraryEvent",cascade = {CascadeType.ALL})
@ToString.Exclude
    protected Book book;
}